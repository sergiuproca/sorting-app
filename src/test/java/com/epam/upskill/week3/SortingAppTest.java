package com.epam.upskill.week3;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;

import java.util.Arrays;

public class SortingAppTest {

    @Test
    public void testNullCase() {
        String[] args = null;
        Assert.assertNull(args);
    }

    @ParameterizedTest
    public static Iterable<Integer[]> lineArguments() {
        return Arrays.asList(new Integer[][]{
                {10, 21, 32, 45},
                {10, 45, 67, 89},
                {45, 56, 78, 62},
                {2, 8, 6, 3}
        });
    }
}