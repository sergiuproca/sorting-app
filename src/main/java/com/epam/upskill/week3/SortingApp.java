package com.epam.upskill.week3;

import java.util.Arrays;

public class SortingApp {
    public static void main(String[] args) {
        int size = args.length;
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = Integer.parseInt(args[i]);
        }
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
    }
}
